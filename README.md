***SQL project with different steps***

**Step 1**

Creating different tables (Departement, Unit, Patient, Stay, Unit Stay).

**Step 2**

Inserting data in those tables.

**Step 3**

Do some calculation and selecting specific data of patients.

**Step 4**

Do some changes (update, delete, join).

**Step 5**

Computation of new fields, some joins, and selecting specific data.


Author : Marion Estoup

E-mail : marion_110@hotmail.fr

November 2020
