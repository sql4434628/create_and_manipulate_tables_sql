-- Author : Marion Estoup
-- E-mail : marion_110@hotmail.fr
-- November 2020


-- DROP TABLE
-- ******************************************************************

drop table if exists UNIT_STAY;
drop table if exists  STAY;
drop table if exists  PATIENT;

drop table if exists  CARE_UNIT;
drop table if exists  UNIT; 
drop table if exists  DEPARTMENT;



-- CREATE TABLE
-- ******************************************************************


-- DEPARTEMENT

CREATE TABLE DEPARTMENT (
--
DEPARTMENT_ID		 			INT 			NOT NULL AUTO_INCREMENT,
DEPARTMENT_CODE					CHAR(4) 		NOT NULL,
DEPARTMENT_LABEL				VARCHAR(50)		NOT NULL,
--
PRIMARY KEY (DEPARTMENT_ID)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
AUTO_INCREMENT=1
;



-- UNIT

CREATE TABLE CARE_UNIT (
--
UNIT_ID		 		INT 			NOT NULL AUTO_INCREMENT,
UNIT_CODE			CHAR(4) 		NOT NULL,
UNIT_LABEL			VARCHAR(50)		NOT NULL,
DEPARTMENT_ID		INT             NOT NULL,
--
PRIMARY KEY (UNIT_ID),
FOREIGN KEY (DEPARTMENT_ID) REFERENCES DEPARTMENT (DEPARTMENT_ID)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
AUTO_INCREMENT=1
;


-- PATIENT

CREATE TABLE PATIENT 
( 
	PATIENT_ID INT NOT NULL AUTO_INCREMENT, 
	FIRST_NAME VARCHAR(50) NOT NULL, 
	LAST_NAME VARCHAR(50) NOT NULL, 
	BIRTH_DATE DATE NOT NULL, 
	SEX CHAR(1) NOT NULL, 
	PRIMARY KEY (PATIENT_ID) 
) 
COLLATE='utf8_general_ci' 
ENGINE=InnoDB 
AUTO_INCREMENT=1
;


-- STAY

CREATE TABLE STAY (
--
STAY_ID		 		INT 			NOT NULL AUTO_INCREMENT,
PATIENT_ID		 	INT 			NOT NULL,
ADMISSION_DATE 	DATE 			NOT NULL,
DISCHARGE_DATE 	DATE 			NOT NULL,
--
PRIMARY KEY (STAY_ID),
FOREIGN KEY (PATIENT_ID) REFERENCES PATIENT (PATIENT_ID)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
AUTO_INCREMENT=1
;



-- UNIT STAY

CREATE TABLE UNIT_STAY (
--
UNIT_STAY_ID		 		INT 			NOT NULL AUTO_INCREMENT,
STAY_ID		 				INT 			NOT NULL,
UNIT_ID 					INT 			NOT NULL,
ADMISSION_DATE 				DATE 			NOT NULL,
DISCHARGE_DATE 				DATE 			NOT NULL,
--
PRIMARY KEY (UNIT_STAY_ID),
FOREIGN KEY (STAY_ID) REFERENCES STAY (STAY_ID),
FOREIGN KEY (UNIT_ID) REFERENCES CARE_UNIT (UNIT_ID)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
AUTO_INCREMENT=1
;


-- INSERT
-- **********************************************************************


-- Insert department

INSERT INTO `DEPARTMENT` (`DEPARTMENT_CODE`, `DEPARTMENT_LABEL`) 
VALUES ('D00A','Department A'),
('D00B','Department B'),
('D00C','Department C'),
('D00D','Department D'),
('D00E','Department E');



-- Insert unit

INSERT INTO `CARE_UNIT`(`UNIT_CODE`, `UNIT_LABEL`, `DEPARTMENT_ID`) 
VALUES ('U0A1','Unit A1', 1),
('U0A2','Unit A2', 1),
('U0A3','Unit A3', 1),
('U0A4','Unit A4', 1),
('U0A5','Unit A5', 1),
('U0B1','Unit B1', 2),
('U0B2','Unit B2', 2),
('U0B3','Unit B3', 2),
('U0B4','Unit B4', 2),
('U0B5','Unit B5', 2),
('U0C1','Unit C1', 3),
('U0C2','Unit C2', 3),
('U0C3','Unit C3', 3),
('U0C4','Unit C4', 3),
('U0C5','Unit C5', 3),
('U0D1','Unit D1', 4),
('U0D2','Unit D2', 4),
('U0D3','Unit D3', 4),
('U0D4','Unit D4', 4),
('U0D5','Unit D5', 4),
('U0E1','Unit E1', 5),
('U0E2','Unit E2', 5),
('U0E3','Unit E3', 5),
('U0E4','Unit E4', 5),
('U0E5','Unit E5', 5);




-- Insertion of records in PATIENT

-- One row

INSERT INTO `PATIENT`(`FIRST_NAME`, `LAST_NAME`, `BIRTH_DATE`, `SEX`) 
VALUES ('First name 1', 'Name 1', '2000-03-30', 'F');

-- Multiples rows

INSERT INTO `PATIENT`(`FIRST_NAME`, `LAST_NAME`, `BIRTH_DATE`, `SEX`) 
VALUES 
	('First name 2', 'Name 2', '1980-02-20', 'F'),
	('First name 3', 'Name 3', '1972-03-05', 'H'),
	('First name 4', 'Name 4', '1938-09-20', 'H'),
	('First name 5', 'Name 5', '1940-10-01', 'F'),
	('First name 6', 'Name 6', '1965-05-06', 'F'),
	('First name 7', 'Name 7', '1967-06-07', 'H'),
	('First name 8', 'Name 8', '1981-03-31', 'F'),
	('First name 9', 'Name 9', '2010-03-12', 'F'),
	('First name 10', 'Name 10', '2020-03-10', 'H');


INSERT INTO `PATIENT`(`FIRST_NAME`, `LAST_NAME`, `BIRTH_DATE`, `SEX`) 
VALUES 
	('First name 11', 'Name 11', '1981-02-20', 'F'),
	('First name 12', 'Name 12', '1973-03-05', 'H'),
	('First name 13', 'Name 13', '1939-09-20', 'H'),
	('First name 14', 'Name 14', '1941-10-01', 'F'),
	('First name 15', 'Name 15', '1967-05-06', 'F'),
	('First name 16', 'Name 16', '1969-06-07', 'H'),
	('First name 17', 'Name 17', '1984-03-31', 'F'),
	('First name 18', 'Name 18', '2011-03-12', 'F'),
	('First name 19', 'Name 19', '2009-03-10', 'H'),
	('First name 20', 'Name 20', '2099-01-01', 'H')
	;



-- STAY

INSERT INTO `STAY`(`PATIENT_ID`, `ADMISSION_DATE`, `DISCHARGE_DATE`) 
VALUES 
	(1, '2020-01-01', '2020-01-05'),
	(1, '2020-01-10', '2020-01-15'),
	(2, '2020-01-01', '2020-01-03'),
	(3, '2020-01-01', '2020-01-04'),
	(3, '2020-01-06', '2020-01-08'),
	(3, '2020-02-20', '2020-03-02'),
	(4, '2020-01-01', '2020-01-03'),
	(4, '2020-02-02', '2020-02-10'),
	(4, '2020-04-01', '2020-04-03'),
	(5, '2020-01-01', '2020-01-04'),
	(6, '2020-01-01', '2020-01-04'),
	(7, '2020-01-01', '2020-01-04'),
	(8, '2020-01-01', '2020-01-04'),
	(9, '2020-01-01', '2020-01-04'),
	(10, '2020-01-01', '2020-01-04'),
	(11, '2020-01-01', '2020-01-04'),
	(12, '2020-01-01', '2020-01-04'),
	(13, '2020-01-01', '2020-01-04'),
	(14, '2020-01-01', '2020-01-04'),
	(15, '2020-01-01', '2020-01-04'),
	(16, '2020-01-01', '2020-01-04'),
	(17, '2020-01-01', '2020-01-04'),
	(18, '2020-01-01', '2020-01-04'),
	(19, '2020-01-01', '2020-01-04'),
	(19, '2020-04-06', '2020-04-08'),
	(19, '2020-05-02', '2020-05-09');




-- INSERT UNIT_STAY

INSERT INTO `UNIT_STAY`(`STAY_ID`, `UNIT_ID`, `ADMISSION_DATE`, `DISCHARGE_DATE`) 
VALUES 	(1, 1, '2020-01-01', '2020-01-05'),
	 	(2, 1, '2020-01-10', '2020-01-12'),
	 	(2, 2, '2020-01-12', '2020-01-15'),
		(3, 1, '2020-01-01', '2020-01-03'),
		(4, 1, '2020-01-01', '2020-01-04'),
		(5, 2, '2020-01-06', '2020-01-08'),
		--
		(6, 1, '2020-02-20', '2020-02-23'),
		(6, 3, '2020-02-23', '2020-02-27'),
		(6, 1, '2020-02-27', '2020-03-02'),
		(7, 1, '2020-01-01', '2020-01-03'),
		(8, 2, '2020-02-02', '2020-02-05'),
		(8, 1, '2020-02-05', '2020-02-10'),
		(9, 4, '2020-04-01', '2020-04-03'),
		(10, 6, '2020-01-01', '2020-01-02'),
		(10, 1, '2020-01-03', '2020-01-04'),
		(11, 6, '2020-01-01', '2020-01-04'),
		(12, 7, '2020-01-01', '2020-01-04'),
		(13, 8, '2020-01-01', '2020-01-04'),
		(14, 9, '2020-01-01', '2020-01-04'),
		(15, 12, '2020-01-01', '2020-01-04'),
		(16, 13, '2020-01-01', '2020-01-04'),
		(17, 12, '2020-01-01', '2020-01-04'),
		(18, 12, '2020-01-01', '2020-01-04'),
		(19, 14, '2020-01-01', '2020-01-04'),
		(20, 1, '2020-01-01', '2020-01-02'),
		(20, 6, '2020-01-02', '2020-01-04'),
		(21, 10, '2020-01-01', '2020-01-03'),
		(21, 1, '2020-01-03', '2020-01-04'),
		(22, 10, '2020-01-01', '2020-01-04'),
		(23, 10, '2020-01-01', '2020-01-04'),
		(24, 14, '2020-01-01', '2020-01-04'),
		(25, 16, '2020-04-06', '2020-04-08'),
		(26, 17, '2020-05-02', '2020-05-09');
		
		
-- Select
-- -------------------------------------------------------------

-- Select all columns of PATIENT

select * from PATIENT;

-- Select first name and last name of PATIENT
select FIRST_NAME, LAST_NAME from PATIENT;

-- Select the patients born in or after 1980
SELECT * FROM PATIENT WHERE year(BIRTH_DATE) >= 1980;
SELECT * FROM PATIENT WHERE BIRTH_DATE >= '1980-01-01';

-- Count the number of rows in PATIENT
SELECT COUNT(*) FROM PATIENT;

-- Count the number of patients per year of birth
SELECT YEAR(BIRTH_DATE), COUNT(*)
FROM PATIENT
GROUP BY YEAR(BIRTH_DATE);

-- Count the number of patients per year of birth
SELECT BIRTH_DATE, COUNT(*)
FROM PATIENT
GROUP BY BIRTH_DATE;

-- Count the number of patient per sex
SELECT SEX, COUNT(*) 
FROM PATIENT 
GROUP BY SEX;

-- Count the number of stay with admission in or after February 2020.
SELECT COUNT(*) AS NB
FROM STAY
WHERE ADMISSION_DATE >= '2020-02-01'

-- Count the number of stay per admission month.
SELECT MONTHNAME(ADMISSION_DATE) AS ADMISSION_MONTH, COUNT(*) AS NB
FROM STAY 
GROUP BY MONTHNAME(ADMISSION_DATE);

-- Update
-- -------------------------------------------------------------

-- Update the birth date of patient 1. New birth_date = '2000-04-30'
UPDATE PATIENT SET BIRTH_DATE = '2000-04-30'
WHERE PATIENT_ID = 1;


-- Delete
-- -------------------------------------------------------------

-- Delete the patient born on 2099-01-01.
DELETE FROM PATIENT 
WHERE BIRTH_DATE = '2099-01-01';



-- Insert
-------------------------------------------------------------

-- Insert a new patient FIRST_NAME_21, NAME_21, '2000-05-06', 'F'
INSERT INTO PATIENT (FIRST_NAME, LAST_NAME, BIRTH_DATE, SEX )
VALUES ('FIRST_NAME_21', 'NAME_21', '2000-05-06', 'F');


-- Join
-- -------------------------------------------------------------

-- Select the admission and discharge date of each patient's stay
SELECT PATIENT.PATIENT_ID, ADMISSION_DATE, DISCHARGE_DATE
FROM PATIENT, STAY
WHERE PATIENT.PATIENT_ID = STAY.PATIENT_ID;

SELECT PATIENT.PATIENT_ID, ADMISSION_DATE, DISCHARGE_DATE
FROM PATIENT INNER JOIN STAY
ON PATIENT.PATIENT_ID = STAY.PATIENT_ID;


-- Count the number of stay per patient
SELECT PATIENT.PATIENT_ID, COUNT(STAY_ID) AS NB_STAY
FROM PATIENT LEFT JOIN STAY
ON PATIENT.PATIENT_ID = STAY.PATIENT_ID
GROUP BY PATIENT.PATIENT_ID;

-- Select the patient who have more than one stay.
SELECT PATIENT.PATIENT_ID, COUNT(STAY_ID) AS NB_STAY
FROM PATIENT LEFT JOIN STAY
ON PATIENT.PATIENT_ID = STAY.PATIENT_ID
GROUP BY PATIENT.PATIENT_ID
HAVING NB_STAY > 1;


-- Select
-------------------------------------------------------------

-- select unit stay in unit U0B1
select UNIT_STAY_id 
from UNIT_STAY us, CARE_UNIT u
where us.unit_id = u.unit_id
and unit_code = 'U0B1';

-- select unit stay in units U0A2 or U0A3
select UNIT_STAY_id 
from UNIT_STAY us, CARE_UNIT u
where us.unit_id = u.unit_id
and (unit_code = 'U0A2' or unit_code = 'U0A3');

select UNIT_STAY_id 
from UNIT_STAY us, CARE_UNIT u
where us.unit_id = u.unit_id
and unit_code in ('U0A2', 'U0A3');

-- select unit stay in department A
select us.UNIT_STAY_id
from UNIT_STAY us, CARE_UNIT u, DEPARTMENT d
where us.unit_id = u.unit_id
and d.department_id = u.department_id
and department_label = 'Department A';


-- Computation of new fields
-- -------------------------------------------------------------


-- compute the duration of each unit stay
select discharge_date - admission_date as duration_stay
from UNIT_STAY;


-- compute the age at the admission date (of the stay)
select year(s.admission_date) - year(p.birth_date) as age_at_admission
from PATIENT p inner join STAY s
on p.patient_id = s.patient_id;

-- compute the average age at the admission date (of the stay)
select AVG(DATEDIFF(s.ADMISSION_DATE , p.BIRTH_DATE )) /365.25 as age
from PATIENT p inner join STAY s
on p.PATIENT_ID = s.PATIENT_ID


-- left outer join
-------------------------------------------------------------

-- number of UNIT_STAY per unit (0 for unit without UNIT_STAY)
select u.UNIT_CODE, count(us.UNIT_STAY_ID)
from CARE_UNIT u left outer join UNIT_STAY us
on us.unit_id = u.unit_id
group by u.UNIT_CODE;

-- number of UNIT_STAY per department (0 for department without UNIT_STAY)



-- advanced sql
-------------------------------------------------------------


-- select the stays with UNIT_STAY in U0A1
select distinct s.STAY_ID
from STAY s inner join UNIT_STAY us
on s.STAY_ID = us.STAY_ID
inner join CARE_UNIT u 
on us.UNIT_ID = u.UNIT_ID
where u.UNIT_CODE = 'U0A1';

select s.STAY_ID
from STAY s inner join UNIT_STAY us
on s.STAY_ID = us.STAY_ID
inner join CARE_UNIT u 
on us.UNIT_ID = u.UNIT_ID
where u.UNIT_CODE = 'U0A1'
group by s.STAY_ID;

select s.*
from STAY s 
where exists (
select * from 
UNIT_STAY us inner join CARE_UNIT u 
on us.UNIT_ID = u.UNIT_ID
where u.UNIT_CODE = 'U0A1'
and s.STAY_ID = us.STAY_ID);

select s.*
from STAY s 
where s.STAY_ID in (
select us.STAY_ID from 
UNIT_STAY us inner join CARE_UNIT u 
on us.UNIT_ID = u.UNIT_ID
where u.UNIT_CODE = 'U0A1');


-- count the stays with UNIT_STAY in U0A1
select count(*)
from STAY s 
where s.STAY_ID in (
select us.STAY_ID from 
UNIT_STAY us inner join CARE_UNIT u 
on us.UNIT_ID = u.UNIT_ID
where u.UNIT_CODE = 'U0A1');

-- select the stays without a unit stay in the unit U0B1

SELECT * 
FROM STAY S 
WHERE NOT EXISTS (
    SELECT *
    FROM UNIT_STAY US, CARE_UNIT U
    WHERE S.STAY_ID = US.STAY_ID 
    AND US.UNIT_ID = U.UNIT_ID
    AND U.UNIT_CODE = 'U0B1'
);


-- select the stays without a unit stay in the unit U0B1 and with a unit stay in the unit U0A1

SELECT * 
FROM STAY s
WHERE NOT EXISTS (
    SELECT *
    FROM UNIT_STAY us, CARE_UNIT u
    WHERE s.STAY_ID = us.STAY_ID 
    AND us.UNIT_ID = u.UNIT_ID
    AND u.UNIT_CODE = 'U0B1'
) and exists (
select * 
from UNIT_STAY us, CARE_UNIT u 
where us.UNIT_ID = u.UNIT_ID
and u.UNIT_CODE = 'U0A1'
and s.STAY_ID = us.STAY_ID
);
